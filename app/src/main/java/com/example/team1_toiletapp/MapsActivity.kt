package com.example.team1_toiletapp


import android.content.Intent
import com.loopj.android.http.*;
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.team1_toiletapp.Common.Common
import com.example.team1_toiletapp.Model.MyPlaces
import com.example.team1_toiletapp.Remote.IGoogleAPIService
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import retrofit2.Call
import retrofit2.Callback
import android.Manifest
import android.view.*
import android.widget.Button
import android.widget.FrameLayout
import com.example.team1_toiletapp.directionhelpers.FetchURL
import com.example.team1_toiletapp.directionhelpers.TaskLoadedCallback
import com.google.android.gms.maps.model.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, TaskLoadedCallback {


    private lateinit var mMap: GoogleMap
    private var latitude: Double = 0.toDouble()
    private var longitude: Double = 0.toDouble()
    private lateinit var mLastLocation: Location
    private var mMarker: Marker? = null

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback

    companion object {
        private const val MY_PERMISSION_CODE: Int = 1000
    }

    private var BUILD_ROUTE: Boolean = false
    private lateinit var button: Button
    private var endPos = LatLng(0.0, 0.0)
    var currentPolyline: Polyline? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkLocationPermission()) {
                buildLocationRequest()
                buildLocationCallBack()

                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
                fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
            }
        } else {
            buildLocationRequest()
            buildLocationCallBack()

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
            )
        }

        mService = Common.googleApiService

        var bottomnav : BottomNavigationView = findViewById<BottomNavigationView>(R.id.BottomNav)
        var frame : FrameLayout = findViewById<FrameLayout>(R.id.frameLayout)

        bottomnav.setOnNavigationItemSelectedListener { item ->
            when(item.itemId) {
                R.id.toilet -> {
                    nearByPlace("toilet")
                }
                R.id.action_addquick -> {
                    val intent2 = Intent(this, AddToiletPin::class.java)
                    startActivity(intent2)
                }
                R.id.report -> {
                    Toast.makeText(this, "Report", Toast.LENGTH_SHORT).show()
                    val intent3 = Intent(this, Report::class.java)
                    startActivity(intent3)
                }
            }
            true
        }

    }

    private fun buildLocationCallBack() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                mLastLocation = p0!!.locations.get(p0.locations.size - 1)

                if (mMarker != null) {
                    mMarker!!.remove()
                }

                latitude = mLastLocation.latitude
                longitude = mLastLocation.longitude

                val latLng = LatLng(latitude, longitude)
                val markerOptions = MarkerOptions()
                    .position(latLng)
                    .title("You are here")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                mMarker = mMap.addMarker(markerOptions)

                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                mMap.animateCamera(CameraUpdateFactory.zoomTo(11f))
                // update polyline while moving
                if (BUILD_ROUTE) {                                                                   // <----
                    createRoute(endPos)
                }
            }
        }
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 10f
    }

    private fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                )
            )
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), MY_PERMISSION_CODE
                )
            else
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), MY_PERMISSION_CODE
                )
            return false
        } else
            return true
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    )
                        if (checkLocationPermission()) {
                            buildLocationRequest()
                            buildLocationCallBack()

                            fusedLocationProviderClient =
                                LocationServices.getFusedLocationProviderClient(this)
                            fusedLocationProviderClient.requestLocationUpdates(
                                locationRequest,
                                locationCallback,
                                Looper.myLooper()
                            )

                            mMap.isMyLocationEnabled = true
                        }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onStop() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        super.onStop()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mMap.isMyLocationEnabled = true
            } else
                mMap.isMyLocationEnabled = true
        }

        mMap.uiSettings.isZoomControlsEnabled = true


        mMap.setMinZoomPreference(6.0f)
        mMap.setMaxZoomPreference(20.0f)

        /* TODO  "Novi" is the button ID for createing route                                        // <---
            "lat" and "lng" is coordinates of destination point (shoud get them from DB)
            for now, destination poin is Riga center
         */

//        button = findViewById(R.id.Novi)
//        button.setOnClickListener{
//            BUILD_ROUTE = true            // make false when destination reached
//            endPos = LatLng (lat, lng)
//            createRoute(endPos)
//        }

        BUILD_ROUTE = true
        googleMap?.apply {
            val riga = LatLng(56.946285, 24.105078)
            addMarker(
                MarkerOptions()
                    .position(riga)
                    .title("Dummy Pin")
            )
            endPos = riga
        }
        createRoute(endPos)

    }


    lateinit var mService: IGoogleAPIService
    internal lateinit var currentPlace: MyPlaces


    private fun nearByPlace(typePlace: String) {

        mMap.clear()

        val url = getUrl(latitude, longitude, typePlace)

        mService.getNearbyPlaces(url)
            .enqueue(object : Callback<MyPlaces> {

                override fun onResponse(
                    call: Call<MyPlaces>,
                    response: retrofit2.Response<MyPlaces>
                ) {
                    currentPlace = response.body()!!

                    if (response.isSuccessful) {
                        for (i in 0 until response.body()!!.results!!.size) {
                            val markerOptions = MarkerOptions()
                            val googlePlace = response.body()!!.results!![i]
                            val lat = googlePlace.geometry!!.location!!.lat
                            val lng = googlePlace.geometry!!.location!!.lng
                            val placeName = googlePlace.name
                            val latLng = LatLng(lat, lng)

                            markerOptions.position(latLng)
                            markerOptions.title(placeName)
                            if (typePlace.equals("toilet"))
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_baseline_wc_24))
                            else
                                markerOptions.icon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_GREEN
                                    )
                                )

                            markerOptions.snippet(i.toString())


                            mMap.addMarker(markerOptions)
                            mMap.moveCamera(
                                CameraUpdateFactory.newLatLng(
                                    LatLng(
                                        latitude,
                                        longitude
                                    )
                                )
                            )
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(11f))

                        }

                    }
                }

                override fun onFailure(call: Call<MyPlaces>, t: Throwable) {
                    Toast.makeText(baseContext, "Nope" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun getUrl(latitude: Double, longitude: Double, typePlace: String): String {
        val googlePlaceUrl =
            StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json")
        googlePlaceUrl.append("?location=$latitude,$longitude")
        googlePlaceUrl.append("&radius=15000")
        googlePlaceUrl.append("&type=$typePlace")
        googlePlaceUrl.append("&key=AIzaSyB4BeFg_6Tuv8ZDD0pWd2UWV9hTZAVooVE")

        Log.d("URL_DEBUG", googlePlaceUrl.toString())
        return googlePlaceUrl.toString()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nearest -> Toast.makeText(this, "Nearest toilet found", Toast.LENGTH_SHORT).show()
            R.id.add -> {
                val intent = Intent(this, AddToilet::class.java)
                startActivity(intent)
            }
            R.id.addPin -> {
                val intent2 = Intent(this, AddToiletPin::class.java)
                startActivity(intent2)
            }
            R.id.report -> {
                Toast.makeText(this, "Report", Toast.LENGTH_SHORT).show()
                val intent3 = Intent(this, Report::class.java)
                startActivity(intent3)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    // updates route line, while moving
    fun createRoute (End: LatLng){
        var start: MarkerOptions = MarkerOptions()
        var end: MarkerOptions = MarkerOptions()

        val myPosition = LatLng (latitude, longitude)
        start = MarkerOptions().position(myPosition)

        // DESTINATION POINT
        end = MarkerOptions().position(End)

        val url: String = getUrl(start.getPosition(), end.getPosition(), "walking")

        FetchURL(this).execute(url, "walking")
//        try {
//            Thread.sleep(5000)
//        } catch (ie: InterruptedException) {
//            Thread.currentThread().interrupt()
//        }
    }

    // creates String for future use
    private fun getUrl(Start: LatLng, End: LatLng, Mode: String): String {                          // <----
        // Origin of route
        val str_origin = "origin=" + Start.latitude + "," + Start.longitude
        // Destination of route
        val str_dest = "destination=" + End.latitude + "," + End.longitude
        // Mode
        val mode = "mode=$Mode"
        // Build parameters to the web service
        val parameters = "$str_origin&$str_dest&$mode"
        // Output format
        val output = "json"
        //Buil the URL to the web service
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" +  //
                parameters + "&key=" + getString(R.string.google_maps_key)
    }

    // draws road on map
    override fun onTaskDone(vararg values: Any?) {                                                  // <----
        if (currentPolyline != null)
            currentPolyline?.remove()
        currentPolyline = mMap.addPolyline(values[0] as PolylineOptions?)
    }
}
