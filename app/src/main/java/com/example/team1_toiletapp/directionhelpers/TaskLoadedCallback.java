package com.example.team1_toiletapp.directionhelpers;


public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
