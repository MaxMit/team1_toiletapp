package com.example.team1_toiletapp.Model

class Photos {
    var height:Int=0
    var width:Int=0
    var html_attributions:Array<String?>?=null
    var photo_reference:String?=null
}