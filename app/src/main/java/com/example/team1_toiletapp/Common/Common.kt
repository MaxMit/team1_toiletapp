package com.example.team1_toiletapp.Common

import com.example.team1_toiletapp.Remote.IGoogleAPIService
import com.example.team1_toiletapp.Remote.RetrofitClient

object Common {
    private val GOOGLE_API_URL="https://maps.googleapis.com/"

    val googleApiService:IGoogleAPIService
        get()=RetrofitClient.getClient(GOOGLE_API_URL).create(IGoogleAPIService::class.java)
}